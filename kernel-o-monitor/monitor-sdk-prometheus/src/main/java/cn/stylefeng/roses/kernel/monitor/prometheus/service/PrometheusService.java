package cn.stylefeng.roses.kernel.monitor.prometheus.service;

import cn.stylefeng.roses.kernel.monitor.api.PrometheusApi;

/**
 * 监控管理prometheus
 *
 * @author chenli
 * @date 2021/1/10 16:09
 */
public interface PrometheusService extends PrometheusApi {

}
