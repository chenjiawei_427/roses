package cn.stylefeng.roses.kernel.demo.constants;

/**
 * demo模块的常量
 *
 * @author fengshuonan
 * @date 2020/10/16 11:05
 */
public interface DemoConstants {

    /**
     * demo模块的名称
     */
    String DEMO_MODULE_NAME = "kernel-s-demo";

    /**
     * 异常枚举的步进值
     */
    String DEMO_EXCEPTION_STEP_CODE = "05";

}
