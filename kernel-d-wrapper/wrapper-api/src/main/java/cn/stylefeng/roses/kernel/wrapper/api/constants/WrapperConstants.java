package cn.stylefeng.roses.kernel.wrapper.api.constants;

/**
 * Wrapper的常量
 *
 * @author fengshuonan
 * @date 2021/1/19 22:23
 */
public interface WrapperConstants {

    /**
     * Wrapper模块的名称
     */
    String WRAPPER_MODULE_NAME = "kernel-d-wrapper";

    /**
     * 异常枚举的步进值
     */
    String WRAPPER_EXCEPTION_STEP_CODE = "24";

}
