package cn.stylefeng.roses.kernel.timer.modular.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.stylefeng.roses.kernel.timer.modular.entity.SysTimers;

/**
 * <p>
 * 定时任务 Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @date 2020/6/30 18:26
 */
public interface SysTimersMapper extends BaseMapper<SysTimers> {

}
