package cn.stylefeng.roses.kernel.config.modular.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.stylefeng.roses.kernel.config.modular.entity.SysConfig;

/**
 * 系统参数配置 Mapper 接口
 *
 * @author stylefeng
 * @date 2019/6/20 13:44
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {


}
