package cn.stylefeng.roses.kernel.dict.modular.mapper;

import cn.stylefeng.roses.kernel.dict.modular.entity.SysDictType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 字典类型表 Mapper 接口
 *
 * @author fengshuonan
 * @date 2020/10/30 21:04
 */
public interface DictTypeMapper extends BaseMapper<SysDictType> {

}
