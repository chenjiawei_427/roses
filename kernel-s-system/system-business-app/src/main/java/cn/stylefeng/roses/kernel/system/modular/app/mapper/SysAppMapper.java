package cn.stylefeng.roses.kernel.system.modular.app.mapper;

import cn.stylefeng.roses.kernel.system.modular.app.entity.SysApp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统应用mapper接口
 *
 * @author fengshuonan
 * @date 2020/3/13 16:17
 */
public interface SysAppMapper extends BaseMapper<SysApp> {
}
