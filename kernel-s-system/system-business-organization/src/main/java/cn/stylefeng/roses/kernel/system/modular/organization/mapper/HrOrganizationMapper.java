package cn.stylefeng.roses.kernel.system.modular.organization.mapper;

import cn.stylefeng.roses.kernel.system.modular.organization.entity.HrOrganization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统组织机构表 Mapper 接口
 *
 * @author fengshuonan
 * @date 2020/11/04 11:05
 */
public interface HrOrganizationMapper extends BaseMapper<HrOrganization> {

}