package cn.stylefeng.roses.kernel.system.modular.organization.mapper;

import cn.stylefeng.roses.kernel.system.modular.organization.entity.HrPosition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统职位表 Mapper 接口
 *
 * @author fengshuonan
 * @date 2020/11/04 11:07
 */
public interface HrPositionMapper extends BaseMapper<HrPosition> {

}