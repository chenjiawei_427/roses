package cn.stylefeng.roses.kernel.system.modular.role.mapper;

import cn.stylefeng.roses.kernel.system.modular.role.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色菜单关联表
 *
 * @author fengshuonan
 * @date 2020/12/19 18:20
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {
}
