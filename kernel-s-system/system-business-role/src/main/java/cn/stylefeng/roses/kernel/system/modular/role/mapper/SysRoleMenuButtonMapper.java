package cn.stylefeng.roses.kernel.system.modular.role.mapper;

import cn.stylefeng.roses.kernel.system.modular.role.entity.SysRoleMenuButton;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色按钮关联 Mapper 接口
 *
 * @author fengshuonan
 * @date 2021/01/09 11:48
 */
public interface SysRoleMenuButtonMapper extends BaseMapper<SysRoleMenuButton> {

}
