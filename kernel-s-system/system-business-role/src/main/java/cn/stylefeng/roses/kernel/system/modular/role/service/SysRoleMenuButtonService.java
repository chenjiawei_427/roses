package cn.stylefeng.roses.kernel.system.modular.role.service;

import cn.stylefeng.roses.kernel.system.modular.role.entity.SysRoleMenuButton;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 角色按钮关联 服务类
 *
 * @author fengshuonan
 * @date 2021/01/09 11:48
 */
public interface SysRoleMenuButtonService extends IService<SysRoleMenuButton> {


}
