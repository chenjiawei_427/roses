package cn.stylefeng.roses.kernel.system.modular.role.service;

import cn.stylefeng.roses.kernel.system.modular.role.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 角色菜单关联信息
 *
 * @author fengshuonan
 * @date 2020/12/19 18:21
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}
