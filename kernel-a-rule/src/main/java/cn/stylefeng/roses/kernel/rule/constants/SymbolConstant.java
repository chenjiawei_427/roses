package cn.stylefeng.roses.kernel.rule.constants;

/**
 * 符号常量
 *
 * @author fengshuonan
 * @date 2021/2/8 11:59
 */
public interface SymbolConstant {

    String COMMA = ",";

    String LEFT_SQUARE_BRACKETS = "[";

    String RIGHT_SQUARE_BRACKETS = "]";

}
