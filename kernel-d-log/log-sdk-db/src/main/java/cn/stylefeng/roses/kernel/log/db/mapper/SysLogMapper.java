package cn.stylefeng.roses.kernel.log.db.mapper;

import cn.stylefeng.roses.kernel.log.db.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 日志记录 Mapper 接口
 *
 * @author luojie
 * @date 2020/11/2 16:22
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
