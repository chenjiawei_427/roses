package cn.stylefeng.roses.kernel.message.db.mapper;

import cn.stylefeng.roses.kernel.message.db.entity.SysMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统消息 Mapper 接口
 *
 * @author liuhanqing
 * @date 2020/12/31 20:09
 */
public interface SysMessageMapper extends BaseMapper<SysMessage> {

}
