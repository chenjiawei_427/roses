package cn.stylefeng.roses.kernel.dsctn.modular.mapper;

import cn.stylefeng.roses.kernel.dsctn.modular.entity.DatabaseInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 数据库信息表 Mapper 接口
 *
 * @author fengshuonan
 * @date 2020/11/18 22:59
 */
public interface DatabaseInfoMapper extends BaseMapper<DatabaseInfo> {

}
