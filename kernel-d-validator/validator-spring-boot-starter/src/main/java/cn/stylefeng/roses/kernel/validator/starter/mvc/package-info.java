/**
 * 这个包内容是对spring mvc原有的一些机制的拓展，为了方便校验器对 RequestGroupContext 之类的上下文添值
 */
package cn.stylefeng.roses.kernel.validator.starter.mvc;
